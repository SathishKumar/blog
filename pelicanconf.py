#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Sathish Kumar'
SITENAME = u'Sathish'
SITEURL = 'https://sathishkumar.gitlab.io/blog/'
SITELOGO = 'https://sathishkumar.gitlab.io/blog/images/profile.png'
PATH = 'content'

TIMEZONE = 'Asia/Kolkata'

DEFAULT_LANG = u'en'

THEME = "pelican-flex"
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Activities','https://sathishkumar.gitlab.io/blog/category/post.html'),
         ('Technical','https://sathishkumar.gitlab.io/blog/category/technical.html'),
         ('Articles','https://sathishkumar.gitlab.io/blog/category/article.html'),
	 ('Python','https://sathishkumar.gitlab.io/blog/category/python.html'),
         ('View all','https://sathishkumar.gitlab.io/blog/archives.html'),
         ('About me', 'https://sathishkumar.gitlab.io/blog/authors.html'),)

# Social widget
SOCIAL = (('facebook', 'https://www.facebook.com/sathish.kokila.9'),
          ('gitlab', 'https://gitlab.com/SathishKumar'),('github','https://github.com/Sathishsathi'))

DEFAULT_PAGINATION = 8

STATIC_PATHS = ['images', 'pdfs']

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
